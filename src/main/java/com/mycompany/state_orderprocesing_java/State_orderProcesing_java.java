package com.mycompany.state_orderprocesing_java;
// Define the State interface
interface OrderState {
    void processOrder(Order order);
    void shipOrder(Order order);
    void deliverOrder(Order order);
}

// Concrete state classes
class PendingState implements OrderState {
    @Override
    public void processOrder(Order order) {
        System.out.println("Processing the order.");
        order.setState(new ProcessingState());
    }

    @Override
    public void shipOrder(Order order) {
        System.out.println("Cannot ship a pending order.");
    }

    @Override
    public void deliverOrder(Order order) {
        System.out.println("Cannot deliver a pending order.");
    }
}

class ProcessingState implements OrderState {
    @Override
    public void processOrder(Order order) {
        System.out.println("Order is already being processed.");
    }

    @Override
    public void shipOrder(Order order) {
        System.out.println("Shipping the order.");
        order.setState(new ShippedState());
    }

    @Override
    public void deliverOrder(Order order) {
        System.out.println("Cannot deliver an order that hasn't been shipped.");
    }
}

class ShippedState implements OrderState {
    @Override
    public void processOrder(Order order) {
        System.out.println("Cannot process a shipped order.");
    }

    @Override
    public void shipOrder(Order order) {
        System.out.println("Order is already shipped.");
    }

    @Override
    public void deliverOrder(Order order) {
        System.out.println("Delivering the order.");
        order.setState(new DeliveredState());
    }
}

class DeliveredState implements OrderState {
    @Override
    public void processOrder(Order order) {
        System.out.println("Cannot process a delivered order.");
    }

    @Override
    public void shipOrder(Order order) {
        System.out.println("Cannot ship a delivered order.");
    }

    @Override
    public void deliverOrder(Order order) {
        System.out.println("Order is already delivered.");
    }
}

// Context class
class Order {
    private OrderState state;

    public Order() {
        state = new PendingState(); // Initial state is "Pending"
    }

    public void setState(OrderState state) {
        this.state = state;
    }

    public void process() {
        state.processOrder(this);
    }

    public void ship() {
        state.shipOrder(this);
    }

    public void deliver() {
        state.deliverOrder(this);
    }
}
public class State_orderProcesing_java {

    public static void main(String[] args) {
        Order order = new Order();
        order.process(); // Output: Processing the order.
        order.ship();    // Output: Shipping the order.
        order.deliver(); // Output: Delivering the order.
        order.ship();    // Output: Cannot ship a delivered order.
    }
}
